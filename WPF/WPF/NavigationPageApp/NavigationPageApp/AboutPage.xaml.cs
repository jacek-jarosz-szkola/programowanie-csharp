﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace NavigationPageApp
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AboutPage : ContentPage
    {

        private double fontSize = 10;
        public double FontSize
        {
            get { return fontSize; }
            set { 
                fontSize = value;
                OnPropertyChanged(nameof(FontSize));
            }
        }



        public AboutPage()
        {
            InitializeComponent();
        }

        private void Button_Clicked(object sender, EventArgs e)
        {
            this.Navigation.PopAsync();
        }

        private void SwipeGestureRecognizer_Swiped(object sender, SwipedEventArgs e)
        {
         //   this.Navigation.PopAsync();
        }

        private void PinchGestureRecognizer_PinchUpdated(object sender, PinchGestureUpdatedEventArgs e)
        {
            FontSize =  Math.Min(Math.Max(fontSize * e.Scale, 10.0), 30.0);            
        }
    }
}