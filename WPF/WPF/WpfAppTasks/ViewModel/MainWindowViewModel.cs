﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using WPFUtilities;

namespace WpfAppTasks.ViewModel
{
    class MainWindowViewModel : ObserverVM
    {
        private int firstValue;
        public int FirstValue
        {
            get { return firstValue; }
            set { firstValue = value; OnPropertyChanged();}
        }

        private int secondValue;
        public int SecondValue
        {
            get { return secondValue; }
            set { secondValue = value; OnPropertyChanged(); }
        }


        private int sumResult;
        public int SumResult
        {
            get { return sumResult; }
            set { sumResult = value; OnPropertyChanged(); }
        }

        private string operationMessage;

        public string OperationMessage
        {
            get { return operationMessage; }
            set { operationMessage = value; OnPropertyChanged(); }
        }


        private ICommand _synchrounousSumCommand;
        public ICommand SynchronousSumCommand
        {
            get
            {
                if (_synchrounousSumCommand == null) 
                {
                    _synchrounousSumCommand = new RelayCommand<object>(arg =>
                       {
                        int result = firstValue + secondValue;
                        Thread.Sleep(10000);
                        SumResult = result;
                        OperationMessage = "Koniec! (sync)";

                       });
                }
                return _synchrounousSumCommand;
            }
        }
 
        private ICommand _taskSumCommand;
        public ICommand TaskSumCommand
        {
            get
            {
                if (_taskSumCommand == null) 
                {
                    _taskSumCommand = new RelayCommand<object>(arg =>
                    {
                        //    Task newTask = new Task(() =>
                        //    {
                        //        int result = firstValue + secondValue;
                        //        Thread.Sleep(10000);
                        //        SumResult = result;
                        //    });
                        //    newTask.Start();

                        Task.Run(() =>
                        {
                            int result = firstValue + secondValue;
                            Thread.Sleep(10000);
                            SumResult = result;
                        });
                        OperationMessage = "Koniec! (async)";




                    });
                }
                return _taskSumCommand;
            }
        }
   
        private ICommand _taskSumTaskMessageCommand;
        public ICommand TaskSumTaskMessageCommand
        {
            get
            {
                if (_taskSumTaskMessageCommand == null) 
                {
                    _taskSumTaskMessageCommand = new RelayCommand<object>(arg =>
                    {
                        //    Task newTask = new Task(() =>
                        //    {
                        //        int result = firstValue + secondValue;
                        //        Thread.Sleep(10000);
                        //        SumTaskMessageResult = result;
                        //    });
                        //    newTask.Start();

                        Task sumTask = new Task(() =>
                        {
                            int result = firstValue + secondValue;
                            Thread.Sleep(10000);
                            SumResult = result;
                        });
                        sumTask.Start();
                        Task.Run(() =>
                        {
                            sumTask.Wait();
                            OperationMessage = "Koniec! (async other)";
                        });
                    });
                }
                return _taskSumTaskMessageCommand;
            }
        }

        private ICommand sum_v2Command;

        private object lockObject = new object();

        public ICommand Sum_v2Command
        {
            get { 

                if (sum_v2Command == null)
                {
                    sum_v2Command = new RelayCommand<object>(async o =>
                    {
                        //lock (lockObject)
                        //{
                        await Task.Run(() =>
                            {
                                int localValue = FirstValue;
                                Thread.Sleep(500);
                                localValue += 2;
                                FirstValue = localValue;
                            });
                        //}

                        //lock (lockObject)
                        //{
                        await Task.Run(() =>
                            {
                                int localValue = FirstValue;
                                Thread.Sleep(500);
                                localValue *= 2;
                                FirstValue = localValue;
                            });
                        //}
                    });
                }
                return sum_v2Command;
            }
        }


    }
}

